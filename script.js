const change = document.querySelectorAll(".change");
const click = document.querySelectorAll(".click");
const reset = document.querySelector(".btn");
let totalBill = 0;
let tip = 0;
let person = 0;
let tipPerson = 0;
let total = 0;

document.querySelector(".disp").innerHTML = "0.00";
document.querySelector(".disp2").innerHTML = "0.00";
reset.addEventListener("click", () => {
  location.reload();
});
change.forEach((val) => {
  val.addEventListener("change", (e) => {
    let value = e.target.name === "tipChange" ? parseFloat(parseInt(e.target.value) / 100) : e.target.value;
    if (e.target.name == "numPeople" && !value) {
      document.getElementById("people").classList.add("borderZero");
      document.querySelector(".cantZero").classList.remove("none");
      return;
    } else if (e.target.name == "numPeople" && value) {
      document.getElementById("people").classList.remove("borderZero");
      document.querySelector(".cantZero").classList.add("none");
    }
    toProcess(value, e.target.name);
  });
});
click.forEach((val) => {
  val.addEventListener("click", (e) => {
    toProcess(e.target.value, e.target.name);
  });
});

const toProcess = (val, name) => {
  totalBill = name === "bill" ? parseFloat(Number(val).toFixed(2)) : totalBill;
  tip = name === "click" || name === "tipChange" ? parseFloat(Number(val).toFixed(2)) : tip;
  person = name === "numPeople" ? parseInt(val) : person;
  console.log(totalBill, tip, person);

  if (totalBill && tip && person) {
    tipPerson = parseFloat(((totalBill / person) * tip).toFixed(2));
    total = (totalBill / person + tipPerson).toFixed(2);
  }

  document.querySelector(".disp").innerHTML = tipPerson ? `${Math.floor((totalBill / person) * tip * 100) / 100}` : 0;
  document.querySelector(".disp2").innerHTML = total ? `${total}` : 0;
};
